$(function(){
    $('#api_settings').DataTable({
        dom: '<"toolbar">frtip',
        paging: true,
        columns: [
            {
                data: 'domain',/*
                render: function(data, type, full, meta) {
                    if (type == 'display') {
                        return data + ' <div class="inline-buttons"><a href="users/edit/'+full.user_id+'">Edit</a> <a href="javascript:void(0);" data-userid="'+full.user_id+'" class="delete">Delete</a></div>'
                    }
                }*/
                class: 'desktop'
            },
            {width: "15%",  data: 'database', class: 'desktop tablet'},
            {width: "20%",  data: 'display_limit',class: 'desktop tablet'},
            {width: "10%",  data: 'active', class: 'desktop'},
        ],
        order:[[1,'asc']],
        responsive: {
            details: {
                type: 'column'
            }
        },
        serverSide: true,
        processing: true,
        select: false,
        ajax: {
            url: ajax_url,
            type: 'POST',
            data:{
                q: 'loadapisettings',
            },
            dataType: 'json'
        },
        "initComplete": function(settings, json) {

        }
    }).on('draw.dt', function(settings){

    }).on('init.dt', function(e, settings, json){
       var toolbarbuttons = [{
                id: 'btn-add-domain',
                href: 'javascript:void(0);',
                icon: 'fa fa-plus-circle',
                label: 'New',
                buttonClass: 'btn-primary',
                click: function() {
                    $('#add-domain').modal('show');
                }
            }, {
                id: 'btn-refresh-upload-table',
                href: 'javascript:void(0);',
                icon: 'fa fa-refresh',
                label: 'Refresh',
                buttonClass: 'btn-success',
                click: function() {
                    $('#api_settings').DataTable().draw();
                }
            }];

        for (x in toolbarbuttons) {
            var button = $('<a class="btn btn-primary" style="margin-right: 5px;"></a');
            var icon = $('<i class="fa" aria-hidden="true"></i>');
            var buttonClass = toolbarbuttons[x].buttonClass != undefined ? toolbarbuttons[x].buttonClass: 'btn-default';
            icon.addClass(toolbarbuttons[x].icon);
            button.attr({
               href:  toolbarbuttons[x].href,
               id: toolbarbuttons[x].id,

            }).append(toolbarbuttons[x].label).prepend(icon).addClass(buttonClass);

            if (toolbarbuttons[x].click != undefined) {
                button.on('click', toolbarbuttons[x].click);
            }

            $('.toolbar').append(button);
        }
        /*
         //Check all/Un-select all Toggle
        $('._checkall').on('change', function(){
            var $me = $(this);
            if ($me.is(':checked')) {
                $('#usermanagement .usermanagement-row-checkbox').prop('checked', true);
            }else{
                $('#usermanagement .usermanagement-row-checkbox:checked').prop('checked', false);
            }
        });
        
        $('.inline-buttons a.delete').on('click', function() {
            var userid = $(this).data('userid'); 
            console.log(userid);
        });*/
    }); 
});

