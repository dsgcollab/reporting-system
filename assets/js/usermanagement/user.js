$(function(){
    $('#usermanagement').DataTable({
        dom: '<"toolbar">frtip',
        paging: true,
        columns: [
            /*
            {
                width: "20px", 
                orderable: false, 
                data: "user_id",
                render: function(data, type, full, meta) {
                    var user_id = data;
                    return '<input type="checkbox" class="usermanagement-row-checkbox" date-userid="'+user_id+'" />'
                },
                className: "dt-body-center"
            },*/
            {
                data: 'full_name',
                render: function(data, type, full, meta) {
                    if (type == 'display') {
                        return data + ' <div class="inline-buttons"><a href="users/edit/'+full.user_id+'">Edit</a> <a href="javascript:void(0);" data-userid="'+full.user_id+'" class="delete">Delete</a></div>'
                    }
                }
            },
            {width: "15%",  data: 'username', visible: false},
            {width: "20%",  data: 'role'},
            {width: "25%",  data: 'group'},
            {width: "15%", data: 'last_login', className: "dt-body-center"},
            {width: "15%",   data: 'date_created', className: "dt-body-center"},
            {
                width: "20px", 
                data: null, 
                orderable: false, 
                searchable: false,
                render: function(data, type, full, meta) {
                    return ''
                },
                visible: false
            }
        ],
        order:[[1,'asc']],
        responsive: true,
        serverSide: true,
        processing: true,
        select: false,
        ajax: {
            url: ajax_url,
            type: 'POST',
            data:{
                q: 'loadusers',
            },
            dataType: 'json'
        },
        "initComplete": function(settings, json) {

        }
    }).on('draw.dt', function(settings){

    }).on('init.dt', function(e, settings, json){
        var toolbarbuttons = [
            {
                id: 'btn-add-user',
                href: '/users/add',
                icon: 'fa fa-user-plus',
                label: 'New',
                buttonClass: 'btn-primary',

            }, {
                id: 'btn-refresh-upload-table',
                href: 'javascript:void(0);',
                icon: 'fa fa-refresh',
                label: 'Refresh',
                buttonClass: 'btn-success',
                click: function() {
                    $('#usermanagement').DataTable().draw();
                }
            }
        ];

        for (x in toolbarbuttons) {
            var button = $('<a class="btn btn-primary" style="margin-right: 5px;"></a');
            var icon = $('<i class="fa" aria-hidden="true"></i>');
            var buttonClass = toolbarbuttons[x].buttonClass != undefined ? toolbarbuttons[x].buttonClass: 'btn-default';
            icon.addClass(toolbarbuttons[x].icon);
            button.attr({
               href:  toolbarbuttons[x].href,
               id: toolbarbuttons[x].id,

            }).append(toolbarbuttons[x].label).prepend(icon).addClass(buttonClass);

            if (toolbarbuttons[x].click != undefined) {
                button.on('click', toolbarbuttons[x].click);
            }

            $('.toolbar').append(button);
        }

         //Check all/Un-select all Toggle
        $('._checkall').on('change', function(){
            var $me = $(this);
            if ($me.is(':checked')) {
                $('#usermanagement .usermanagement-row-checkbox').prop('checked', true);
            }else{
                $('#usermanagement .usermanagement-row-checkbox:checked').prop('checked', false);
            }
        });
        
        $('.inline-buttons a.delete').on('click', function() {
            var userid = $(this).data('userid'); 
            console.log(userid);
        });
    });
});