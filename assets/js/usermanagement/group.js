$(function(){
   $('#group').DataTable({
       dom: '<"toolbar">frtip',
       paging: true,
       columns:[
           {data: 'group'},
           {data: 'users_count', width: '250px'},
           {data: 'active', width: '10px', className: "dt-body-center"}
       ],
       ajax: {
            url: ajax_url,
            type: 'POST',
            data:{
                q: 'loadgroups',
            },
            dataType: 'json'
        }
   });
});


