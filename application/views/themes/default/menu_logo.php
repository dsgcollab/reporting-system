<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<nav class="cm-navbar cm-navbar-primary">
    <div class="cm-flex">
        <a href="/" class="cm-logo"></a>
    </div>
    <div class="btn btn-primary md-menu-white" data-toggle="cm-menu"></div>
</nav>
