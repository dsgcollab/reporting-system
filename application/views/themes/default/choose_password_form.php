<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    $showform = 1;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Account Recovery - Change Password</title>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon.png'); ?>">
        <meta name="robots" content="noindex, nofollow">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        
        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    </head>
    <body>
        <!-- Logo Top -->
        <div class="text-center" style="padding:90px 0 30px 0;background:#fff;border-bottom:1px solid #ddd">
            <img src="/assets/img/ds_logo.png" />
        </div>
        <!-- Logo Top -->
        <div class="container">
        <?php if ( isset($validation_errors) ) { ?>
        <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <div class="panel panel-danger">
                <div class="panel-heading">Form Validation Error</div>
                <div class="panel-body">
                    <p>
                        The following error occurred while changing your password
                    </p>
                    <ul>
                        <?php echo $validation_errors; ?>
                    </ul>
                    <p>
                            Password not updated
                    </p>
                </div>
            </div>
        </div>
        <?php }else{ $display_instructions = 1; } ?>
            
        <?php if ( isset( $validation_passed ) ) { ?>
        <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <div class="panel panel-success">
                <div class="panel-heading">Password Changed</div>
                <div class="panel-body">
                    <p>
                       You have successfully changed your password
                    </p>
                    <p>
                       You can now <a href="<?php echo site_url(LOGIN_PAGE);  ?>">login</a>
                    </p>
                </div>
            </div>
        </div>
        <?php $showform = 0;} ?>
        
        <?php if( isset( $recovery_error ) ) { ?>
        <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <div class="panel panel-danger">
                <div class="panel-heading">Password Changed</div>
                <div class="panel-body">
                    <p>
				No usable data for account recovery.
			</p>
			<p>
				Account recovery links expire after 
				<?php echo ( (int) config_item('recovery_code_expiration') / ( 60 * 60 ) ); ?> 
				hours.<br />You will need to use the 
				<a href="/examples/recover">Account Recovery</a> form 
				to send yourself a new link.
			</p>
                </div>
            </div>
        </div>
        <?php $showform = 0;} ?>
        <?php if (isset( $disabled )) { ?>
        <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <div class="panel panel-danger">
                <div class="panel-heading">Account recovery is disabled</div>
                <div class="panel-body">
			<p>
				You have exceeded the maximum login attempts or exceeded the 
				allowed number of password recovery attempts. 
				Please wait <?php echo  ( (int) config_item('seconds_on_hold') / 60 ); ?> 
				minutes, or contact us if you require assistance gaining access to your account.
			</p>
                </div>
            </div>
        </div>
        <?php $showform = 0; } ?>
 <?php
 if( $showform == 1 ) { ?>
    
        <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <div class="panel panel-default">
                <div class="panel-heading">Choose your new password</div>
                <div class="panel-body">
                    <?php
     if( isset( $recovery_code, $user_id ) ) {
         if ( isset( $display_instructions ) ) {
            if( isset( $username ) ) {
 ?>
        <div class="alert alert-info alert-dismissable fade in shadowed" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-fw fa-lock"></i> Your login user name is <i><?php echo  $username; ?></i><br />
					Please write this down, and change your password now:
        </div>
 <?php
            }else{
             ?>
        <div class="alert alert-danger alert-dismissable fade in shadowed" role="alert">Please change your password now:</div>
        <?php
            }
         }
?>      
                    
                    <?php echo form_open(); 
                    echo form_hidden('recovery_code',$recovery_code);
                    echo form_hidden('user_identification',$user_id);
                    ?>
                    
                    <div class="form-group">
                        <label>Password</label>
                        <?php
                        $input_data = [
                                'name'       => 'passwd',
                                'id'         => 'passwd',
                                'class'      => 'form_input password form-control',
                                'max_length' => config_item('max_chars_for_password')
                        ];
                        echo form_password($input_data);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>
                            Confirm Password
                        </label>
                        <?php
                        $input_data = [
                                'name'       => 'passwd_confirm',
                                'id'         => 'passwd_confirm',
                                'class'      => 'form_input password form-control',
                                'max_length' => config_item('max_chars_for_password')
                        ];
                        echo form_password($input_data);
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                        $input_data = [
									'name'  => 'form_submit',
									'id'    => 'submit_button',
									'value' => 'Change Password',
                                                                        'class' => 'btn btn-primary'
								];
								echo form_submit($input_data);
                        ?>
                    </div>
          
                </div>   
            </div>
        </div>    
         
<?php         
     }
 }
 ?>
 </div>
        <!-- Recovery Form -->
        <!-- Theme Script -->
        <script src="<?php echo base_url('assets/js/jquery.mousewheel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.cookie.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/clearmin.min.js'); ?>"></script>
        <!-- Theme Script -->
    </body>
</html>