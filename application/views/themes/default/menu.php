<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$uri_string = uri_string();

?>
<ul class="cm-menu-items">
<?php if ( isset( $menu_list ) && $menu_list ): ?>
    <?php foreach($menu_list as $menu_item): ?>
        <?php if ($menu_item['children']) { ?>
            <?php
                $add_class = '';
                foreach($menu_item['children'] as $child_menu_item)
                {   
                    if ($child_menu_item['slug'] == $uri_string)
                    {
                        $add_class = 'pre-open';
                        break;
                    }    
                }    
            ?>
            <li class="cm-submenu <?php echo $add_class; ?>">
                <a class="sf-cogs" <?php echo $menu_item['slug'] == '#' || $menu_item['slug'] == ''? '':'href="' . base_url($menu_item['slug'] . '"'); ?>><?php echo $menu_item['title']; ?> <span class="caret"></span></a>
                <ul>
                    <?php foreach($menu_item['children'] as $child_menu_item): ?>
                        <li class="<?php echo $child_menu_item['slug'] == $uri_string?'active':''; ?>"><a href="<?php echo base_url($child_menu_item['slug']); ?>"><?php echo $child_menu_item['title']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php }else{ ?>
            <?php
            $class = $menu_item['slug'] == $uri_string ? 'active':'';
            ?>
            <li class="<?php echo $class; ?>"><a href="<?php echo base_url($menu_item['slug']); ?>" class="<?php echo isset($menu_item['icon']) && $menu_item['icon']?trim($menu_item['icon']):'sf-dashboard-alt';?>"><?php echo $menu_item['title']; ?></a></li>
        <?php } ?>
    <?php endforeach; ?>
<?php endif; ?>
</ul>