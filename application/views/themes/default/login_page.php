<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>System Login</title>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon.png'); ?>">
        <meta name="robots" content="noindex, nofollow">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        
        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    </head>
    <body id="cm-login">
        
        <!-- Logo Top -->
        <div class="text-center" style="padding:90px 0 30px 0;background:#fff;border-bottom:1px solid #ddd">
            <img src="assets/img/ds_logo.png" />
        </div>
        <!-- Logo Top -->
   
        <!-- Login Form -->
        <div class="col-sm-6 col-md-4 col-lg-3" style="margin:40px auto; float:none;">
           
            <!-- Error Message -->
            <?php if (isset($error_message) && $error_message): ?>
            <div class="alert alert-danger alert-dismissable fade in shadowed" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-fw fa-lock"></i> Login Error
                <p>
                <?php echo $error_message; ?>
                </p>
            </div>
            <?php endif; ?>
   
            <?php if ( $logged_out_message !== FALSE): ?>
            <div class="alert alert-info alert-dismissable fade in shadowed" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><i class="fa fa-fw fa-info-circle"></i> <?php echo $logged_out_message; ?>
            </div>
            <?php endif; ?>
            <!-- Error Message -->
            <?php echo form_open('login', array()); ?>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-fw fa-user"></i></div>
                                <input type="text" name="identity" class="form-control" placeholder="Username/Email" value="<?php echo set_value('identity', ''); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-fw fa-lock"></i></div>
                            
                            <input type="password" class="form-control" placeholder="Password" name="password"  />
                        </div>
                    </div>
                </div>
                <?php if (config_item('allow_remember_me')) { ?>
                <div class="col-xs-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="remember_me" name="remember_me" value="yes"> Remember me
                        </label>
                    </div>
                </div>
                <?php } ?>
                <div class="col-xs-6">
                        <button type="submit" class="btn btn-block btn-primary">Login</button>
                </div>            
            </form>
        </div>
        <!-- Login Form -->
        
        <!-- Theme Script -->
        <script src="<?php echo base_url('assets/js/jquery.mousewheel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.cookie.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/clearmin.min.js'); ?>"></script>
        <!-- Theme Script -->
    </body>
</html>