<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Account Recovery</title>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon.png'); ?>">
        <meta name="robots" content="noindex, nofollow">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        
        <!-- Theme CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    </head>
    <body>
        <div class="text-center" style="padding:90px 0 30px 0;background:#fff;border-bottom:1px solid #ddd">
            <img src="<?php echo base_url('assets/img/ds_logo.png') ?>" />
        </div>

        <div class="container">
            <div class="col-sm-12 col-md-8 col-lg-8" style="margin:40px auto; float:none;">
            <?php
                //Disabled
                $seconds_on_hold = ( (int) config_item('seconds_on_hold') / 60 );
                if ( isset($disabled) && $disabled == 1 ) {
            ?>
            <div class="panel panel-danger">
                <div class="panel-heading">Account Recovery is Disabled</div>
                <div class="panel-body">
                    <p>
                    If you have exceeded the maximum login attempts, or exceeded
                    the allowed number of password recovery attempts, account recovery 
                    will be disabled for a short period of time. </p>
                    <p>
                    Please wait <?php echo $seconds_on_hold; ?> 
                    minutes, or contact us if you require assistance gaining access to your account.
                    </p>
                   
                </div>
            </div>
            <?php }else if( isset($banned) && $banned == 1 ){ ?>
            <div class="panel panel-danger">
                <div class="panel-heading">Account Locked</div>
                <div class="panel-body">
                    <p>
                        You have attempted to use the password recovery system using an email address that belongs to an account that has been purposely denied access to the authenticated areas of this website. 
				If you feel this is an error, you may contact us  
				to make an inquiry regarding the status of the account.
                    </p> 
                </div>
            </div>
            <?php }else if( isset($confirmation) && $confirmation == 1 ) { ?>
                <div class="panel panel-success">
                    <div class="panel-heading">Recovery Email Sent</div>
                    <div class="panel-body">
                        <p>
                            We have sent you an email with instructions on how to recover your account.
                        </p>
                        <p>Link: <?php echo $special_link; ?>
                    </div>
                </div>
            <?php }else if ( isset($no_match) && $no_match == 1 ) { ?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Supplied email did not match any record.
                </div>
            <?php 
                $show_form = 1;
            }else{ ?>
                <div class="alert alert-info" role="alert">
                    <span class="glyphicons glyphicons-remove-circle" aria-hidden="true"></span>
                    <p>
                        If you've forgotten your password and/or username, 
			enter the email address used for your account, 
			and we will send you an e-mail 
			with instructions on how to access your account.
                    </p>
                </div>
            <?php 
                $show_form = 1;
            } ?>
            </div>
            
            <?php if ( isset( $show_form ) && $show_form == 1 ) { ?>
            <div class="col-sm-12 col-md-6 col-lg-4" style="margin:40px auto; float:none;">
                <div class="panel panel-info">
                    <div class="panel-heading">Enter your account's email address</div>
                    <div class="panel-body">
                        <?php echo form_open('', array()); ?>
                        <div class="form-group">
                            <input type="text" id="email" name="email" class="form-control" maxlength="2550" placeholder="Account's Email Address"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary">Submit</button>
                        </div>
                        <?php echo form_close(); ?>                        
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        
        <!-- Theme Script -->
        <script src="<?php echo base_url('assets/js/jquery.mousewheel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.cookie.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.min.js'); ?>"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/js/clearmin.min.js'); ?>"></script>
        <!-- Theme Script -->
    </body>
</html>