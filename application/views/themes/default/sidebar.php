<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="cm-menu">
    <?php ci_get_logo(); ?>
    <div id="cm-menu-content">
        <div id="cm-menu-items-wrapper">
            <div id="cm-menu-scroller">
                <?php echo create_menu('Main'); ?>
            </div>
        </div>
    </div>
</div>  

