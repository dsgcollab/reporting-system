<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header id="cm-header">
    <nav class="cm-navbar cm-navbar-primary">
        <div class="btn btn-primary md-menu-white hidden-md hidden-lg" data-toggle="cm-menu"></div>
        <div class="cm-flex">
            <h1><?php echo isset($page_header_title) ? $page_header_title:''; ?></h1> 
            <!--<form id="cm-search" action="index.html" method="get">
                <input type="search" name="q" autocomplete="off" placeholder="Search...">
            </form>-->
        </div>
        <!--
        <div class="pull-right">
            <div id="cm-search-btn" class="btn btn-primary md-search-white" data-toggle="cm-search"></div>
        </div>-->
        <div class="dropdown pull-right">
            <button class="btn btn-primary md-account-circle-white" data-toggle="dropdown"></button>
            <ul class="dropdown-menu">
                <!--
                <li class="disabled text-center">
                    <a style="cursor:default;"><strong>User<?php //echo $first_name; ?></strong></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
              
                <li>
                    <a href="#"><i class="fa fa-fw fa-cog"></i> Settings</a>
                </li>
                -->
                <li>
                    <a href="<?php echo site_url('logout'); ?>"><i class="fa fa-fw fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--
    <nav class="cm-navbar cm-navbar-default cm-navbar-slideup">
        <div class="cm-flex">
            <?php //echo isset($breadcrumbs) ? $breadcrumbs:''; ?>
        </div>
    </nav>
    -->
</header>


