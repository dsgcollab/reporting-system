<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <meta name="robots" content="noindex, nofollow">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicon.png'); ?>">
        <script src="<?php echo base_url('assets/system.js'); ?>"></script>
        <title><?php echo $title; ?></title>        
        
        <!-- Theme CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-clearmin.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/roboto.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/material-design.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/small-n-flat.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
        <!-- Theme CSS -->
        
        <!-- Head Add CSS -->
        <?php
            if ( isset($head_css) && $head_css ) {
                foreach($head_css as $css) {
                    echo link_tag($css['href']);
                }
            }
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css'); ?>">
        
        <!-- Head Add Script -->
        <script src="<?php echo base_url('assets/js/lib/jquery-2.1.3.min.js'); ?>"></script>
        <?php
            if ( isset($head_script) && $head_script ) {
                foreach($head_script as $script) {
                    ?>
         <script type="text/javascript" src="<?php echo $script['src']; ?>"></script>
                    <?php
                }
            }
        ?>
    </head>
    
    <body class="cm-no-transition cm-1-navbar">
        <?php ci_get_sidebar(); ?>
        <?php ci_get_header(); ?>
        <div id="global">
            <div class="container-fluid">
                <?php echo isset($content) ? $content:''; ?>
            </div>
            <?php ci_get_footer(); ?>
        </div>
        
         <!-- Foot Add CSS -->
        <?php
            if ( isset($foot_css) && $foot_css ) {
                foreach($foot_css as $css) {
                    echo link_tag($css['href']);
                }
            }
        ?>
        
        <!-- Theme Script -->
        <script src="<?php echo base_url('assets/js/jquery.mousewheel.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/js.cookie.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.min.js'); ?>"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('assets/js/clearmin.min.js'); ?>"></script>
        
        <!-- Theme Script -->
        
        <!-- Foot Add Script -->
        <?php
            if ( isset($foot_script) && $foot_script ) {
                foreach($foot_script as $script) {
                    ?>
    <script type="text/javascript" src="<?php echo $script['src']; ?>"></script>
                    <?php
                }
            }
        ?>
    </body>
</html>
