<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!--<div class="row cm-fix-height">
    <div class="col-sm-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                SEMrush API Units Left
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <span class="semrush-units-left-number"><?php echo isset($unitsleft) ?number_format($unitsleft):''; ?> </span>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div class="row cm-fix-height">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Domain Organic Search Cron Jobs 
            </div>
            <div class="panel-body">
                 <table id="api_settings" class="table table-striped table-bordered table-list hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Domain</th>
                        <th>Date Added</th>
                        <th>Recurring Type</th>
                        <th>Active</th>
                    </tr> 
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="add-domain">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Add domain</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php echo isset($addform)?$addform:''; ?>