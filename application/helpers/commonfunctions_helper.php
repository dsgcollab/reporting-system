<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if ( !function_exists('create_menu') ) {
    function create_menu($menu_name)
    {
        $CI =& get_instance();
        $CI->load->model('menu_model');
        $menu_list = $CI->menu_model->get_menu($menu_name);
           
        if ( $menu_list )
        {
            return $CI->load->view('themes/default/menu', array('menu_list' => $menu_list), TRUE);
        }
    }
}

if ( !function_exists('user_roles')) {
    function user_roles() {
        $CI =& get_instance();
        $CI->load->model('role_model', 'role');
        
        return $CI->role->get_roles();
    }
}

if ( !function_exists('ci_add_css') ) {
    function ci_add_css( $add_css ) {
        $CI =& get_instance();
        $head_css   = array();
        $foot_css = array();
        
        if ($add_css) {
            foreach($add_css as $css) {
               if ( $css['position'] == 'head' ) {
                   $head_css[] = $css;
               }else{
                   $foot_css[] = $css;
               }
            }
        }
        
        if ($head_css) {
            $CI->load->vars(array('head_css' => $head_css));
        }
        
        if ($foot_css) {
            $CI->load->vars(array('foot_css' => $foot_css));
        }
    }
}

if ( !function_exists('ci_add_script') ) {
    function ci_add_script( $add_script ) {
        $CI =& get_instance();
        $head_script = array();
        $foot_script = array();
        if ($add_script) {
            foreach($add_script as $script) {
                if ($script['position'] == 'head') {
                    $head_script[] = $script;
                }else{
                    $foot_script[] = $script;
                }
            }
        }
        
        if ($head_script) {
            $CI->load->vars(array('head_script' => $head_script));
        }
        
        if ( $foot_script ) {
            $CI->load->vars(array('foot_script' => $foot_script));
        }
    }
}

if ( !function_exists('ci_get_header') )
{
    function ci_get_header()
    {
        $_ci = & get_instance();
        $theme = $_ci->config->item('theme');
        
        $_ci->load->view($theme . '/header', NULL);
    }
}

if ( !function_exists('ci_get_footer') )
{
    function ci_get_footer($data = NULL)
    {
        $_ci = & get_instance();
        $theme = $_ci->config->item('theme');
        $_ci->load->view($theme . '/footer', $data);
    }
}

if ( !function_exists('ci_get_sidebar') )
{
    function ci_get_sidebar($data = NULL)
    {
        $_ci = & get_instance();
        $theme = $_ci->config->item('theme');
        $_ci->load->view($theme . '/sidebar', $data);
    }
}

if ( !function_exists('ci_get_logo') ) 
{
    function ci_get_logo()
    {
        $_ci = & get_instance();
        $theme = $_ci->config->item('theme');
        
        $_ci->load->view($theme . '/menu_logo', NULL);
    }
}