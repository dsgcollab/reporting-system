<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $force_logged_in = TRUE;
    
    //Template variables
    public $page_title;
    public $h1_title;
    public $content;
    public $template_data;
    
    public function __construct()
    {
        parent::__construct();
        if ($this->ion_auth->logged_in() == FALSE) redirect('/login');
    }
    
    public function render()
    {
        $this->load->vars(array(
            'title'             => $this->page_title,
            'page_header_title' => $this->h1_title,
            'content'           => $this->content
        ));
        
        $this->template->render_page();
    }
    
    public function render_page() 
    {
        $this->template->page_title         = $this->page_title;
        $this->template->page_header_title  = $this->h1_title;

        $this->template->content  = $this->content;
        $this->template->load();
    }
}