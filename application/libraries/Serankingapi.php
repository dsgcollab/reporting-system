<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Serankingapi
{   
    protected $_ci;
    protected $token;
    private $api_url;
    
    public function __construct() 
    {
        $this->_ci =& get_instance();
        $this->api_key = $this->_ci->config->item('seranking_api_url');
    }
    
    private function _curlcall($method = 'GET', $data)
    {
        $url = $method == 'GET' ? $this->api_url .'?'. http_build_query($data):$this->api_url;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        if ($method == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if ($httpcode != 200) {
            return FALSE;
        }
        
        if ($output)
        {
            return $output;
        }
        
        return FALSE;
    }
    
    //Return Token
    private function _authenticate()
    {
        $access_detail = $this->_ci->config->item('seranking_login_details');
        
        $username = isset($access_details['username'])?$access_details['username']:FALSE;
        $password = isset($access_details['password'])?$access_details['password']:FALSE;
        
        if ($username !== FALSE && $password !== FALSE)
        {
            $token = $this->_curlcall('GET', array('login' => $username, 'pass' => $password));
        }
        
        return FALSE;
    }
    
}


