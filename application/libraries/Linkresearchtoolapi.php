<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Linkresearchtoolapi
{
    protected $_ci;
    private $api_key;
    private $api_url;
    private $report_id;


    public function __construct() 
    {
        $this->_ci =& get_instance();
        $this->api_key = $this->_ci->config->item('linkresearchtool_api_key');
        $this->api_url = $this->_ci->config->item('linkresearchtool_api_url');
    }
    
    public function __get($name)
    {
        switch($name)
        {
            case 'api_key':
                return $this->api_key;
        }
    }
    
    public function extract_data_report($action, $report_id)
    {
        $post_data = array(
            'action'    => $action,
            'report_id' => $report_id
        );
        
        $html = $this->_curlcall($post_data);
        return $html;
    }
    
    private function _curlcall($additional_settings)
    {
        $request_data = array(
            'api_key'   => $this->api_key
        );
        
        $post_data = array_merge($request_data, $additional_settings);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        echo var_dump($output);
        echo var_dump($httpcode);
        if ($httpcode != 200) {
            return FALSE;
        }
        
        if ($output)
        {
            return $output;
        }
        
        return FALSE;
    }
}
