<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Template
{
    private $CI;
    private $page_title = '';
    private $page_header_title = '';
    public $template_data = [];
    public $contentTemplate = '';
    public $content;
    public $content_data = [];
    
    private $theme;
    public $breadcrumbs = '';
    
    public function __construct() 
    {
        $this->CI =& get_instance();
        $this->theme = $this->CI->config->item('theme');
    }
    
    public function render_page()
    {  
        $this->CI->load->view($this->theme . '/page', NULL);
    }
    
    public function __set($name, $value)
    {
        switch($name)
        {
            case 'theme':
                $this->theme = $value;
                break;
            default:
                $this->$name = $value;
                break;
        }
    }

    public function __get($name)
    {
        switch($name)
        {
            case 'theme':
                return $this->theme;
            default:
                return $this->$name;
                break;
        }
    }
    
    private function _generate_content()
    {
        $blank_page = $this->theme . '/_blankcontent';
        
        
    }
    
    private function _generate_footer()
    {
        return $this->load->view($this->theme . '/footer', NULL, TRUE);
    }
    
    private function _generate_header()
    {
        $data = array(
            'page_header_title' => $this->page_header_title
        );
        
        return $this->load->view($this->theme . '/header', $data, TRUE);
    }
    
    public function load($data = array())
    {
        $content_template = !empty($this->content)?$this->content:$this->theme . '/_blankcontent';
        
        //Set the header
        $this->template_data['header'] = $this->header();
        //Set the footer
        $this->template_data['footer'] = $this->footer();
        //Set Sidebar template
        $this->template_data['sidebar'] = $this->sidebar();
        //Set Menu template
        $this->template_data['menu'] = $this->menu();
        
        //Set the content
        $this->template_data['content'] = $this->CI->load->view($content_template, $data, TRUE);
        
        $this->CI->load->view($this->theme . '/page', $this->template_data);
    }
    
    private function header($data = array())
    {
        $data['title']              = $this->page_title;
        $data['page_header_title']  = $this->page_header_title;
        
        $data['breadcrumbs'] = $this->CI->load->view($this->theme . '/breadcrumbs', array('output' => $this->breadcrumbs), TRUE);
        
        return $this->CI->load->view($this->theme . '/header', $data, TRUE);
    }
    
    private function footer($data = array())
    {
        return $this->CI->load->view($this->theme . '/footer', $data, TRUE );
    }
    
    private function menu($data = array())
    {
        $this->CI->load->library('Multi_menu');
        $this->CI->load->model('menu_model', 'menu');
        
        $menu_list = $this->CI->menu->all();
        $this->CI->multi_menu->set_items($menu_list);
        $menu_output = $this->CI->multi_menu->render();
        return $this->CI->load->view($this->theme . '/menu', $data, TRUE );
    }
    
    private function sidebar($data = array())
    {
        $data['menu_logo'] = $this->CI->load->view($this->theme . '/menu_logo', array(), TRUE);
        
        $data['menu'] = create_menu('Main');
        
        return $this->CI->load->view($this->theme . '/sidebar', $data, TRUE );
    }        
}
