<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Semrushapi
{   
    private $table_name;
    protected $_ci;
    protected $api_key;
    
    public function __construct() 
    {
        $this->_ci =& get_instance();
        $this->api_key = $this->_ci->config->item('semrush_api_key');
    }
    
    
    public function analytics_api($api)
    {
        switch($api)
        {
            case 'analytics_api':
                
                break;
        }
    }
    
    public function domain_reports($report_type = NULL, $settings = array())
    {
        if ($report_type)
        {
            switch($report_type)
            {
                case 'domain_organic':
                    $api_url        = $this->_ci->config->item('semrush_api_url');
                    $api_column     = $this->_ci->config->item('domain_organic_api_column_list');
                    
                    $domain             = isset($settings['domain'])?$settings['domain']:FALSE;
                    $display_limit      = isset($settings['display_limit'])?$settings['display_limit']:10;
                    $google_database    = isset($settings['database'])?$settings['database']:FALSE;
                    
                    $url_param = array(
                        'type'              => 'domain_organic',
                        'export_columns'    => implode(array_keys($api_column), ','),
                        'key'               => $this->api_key
                    );
                    
                    $url_param['display_limit'] = $display_limit;
                    $url_param['domain']        = $domain;
                    $url_param['database']      = $google_database;
                    
                    $api_url .= ('?' . http_build_query($url_param));
                    $html = $this->_curlcall($api_url);
                    
                    if ($html !== FALSE || ! empty($html))
                    {
                        $report_data = $this->parsecsv($html, ';');
                        if (is_array($report_data) && !empty($report_data))
                        {
                            $this->_ci->load->model('domainorganic_model', 'do');
                            $this->_ci->do->table_name = $domain;
                            $this->_ci->do->save_report($report_data);
                        }
                    }
                    break;
            }
        }
        
        return FALSE;
    }
    
    public function accounts_api($report_type)
    {
        switch($report_type)
        {
            case 'check_api_units_balance':
                $api_url = 'http://www.semrush.com/users/countapiunits.html';
                $url_param = array(
                    'key' => $this->api_key
                );
                
                $api_url .='?' . http_build_query($url_param);
                $result = $this->_curlcall($api_url);
                return $result;
                break;
        }
        
        return FALSE;
    }
    
    private function _curlcall($url)
    {
        $tmpfname = FCPATH.'cookie.txt';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $tmpfname);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $tmpfname);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        $html = $output;
        curl_close($ch);
        
        if ($httpcode != 200)
        {
            return FALSE;
        }
        
        if ($output)
        {
            return $html;
        }
        
        return FALSE;
    }
    
    public function parsecsv($csv_text = '', $separator = ',')
    {
        if (empty($csv_text) || !is_string($csv_text)) 
        {
            return FALSE;
        }
        
        $lines = str_getcsv($csv_text, "\n\r");
        if ($lines)
        {
            $return_data = array();
            $header = str_getcsv(array_shift($lines), $separator);
            foreach($lines as $i => $line)
            {
                $values = str_getcsv($line, $separator);
                if (is_array($values) && ! empty($values))
                {
                    $return_data[] = array_combine($header, $values);
                }
            }
            
            return $return_data;
        }
        
        return FALSE;
    }
}


