<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Breadcrumb {
 private $breadcrumbs = array();
 private $separator = '  >  ';
 private $start = '<div class="cm-breadcrumb-container"><ol class="breadcrumb">';
 private $end = '</ol></div>';

 public function __construct($params = array()){
  if (count($params) > 0){
   $this->initialize($params);
  }  
 }
 
 private function initialize($params = array()){
  if (count($params) > 0){
   foreach ($params as $key => $val){
    if (isset($this->{'_' . $key})){
     $this->{'_' . $key} = $val;
    }
   }
  }
 }

 function add($title, $href){  
  if (!$title OR !$href) return;
  $this->breadcrumbs[] = array('title' => $title, 'href' => $href);
 }
 
 function output(){

  if ($this->breadcrumbs) {
   
   $output = $this->start;

   foreach ($this->breadcrumbs as $key => $crumb) {
   
    $list = array_keys($this->breadcrumbs);
    if (end($list) == $key) {
     $output .= '<li class="active">' . $crumb['title'] . '</li>';   
    } else {
     $output .= '<li><a href="' . $crumb['href'] . '">' . $crumb['title'] . '</a></li>';
    }
   }
  
   return $output . $this->end . PHP_EOL;
  }
  
  return '';
 }

}
