<?php
 class Cronschedule_model extends CI_Model
 {
     public $table_name = '';
     protected $json_settings;
     
     private $recurring_type_list = array(
         'monthly',
         'daily',
         'weekly'
     );
     
     public function __construct() 
     {
         parent::__construct();
         
     }
     
     public function getunistleft()
     {
         $this->db->select();
         $this->db->from('unitscount');
         $rs = $this->db->get();
         if ($rs->num_rows() > 0)
         {
             return $rs->row()->unitsleft;
         }
         
         return FALSE;
     }
     
     public function apisettings()
     {
        $table_name = 'domain_api_settings'; 
        
        $start          = (int) $this->input->post('start'); //Page (Integer)
        $limit          = (int) $this->input->post('length'); //Limit per page (Integer)
        $sort_order     = $this->input->post('order'); //Sort Order (String)
        $search         = $this->input->post('search'); //Search (String)
      
        $db_fields = array(
            'SQL_CALC_FOUND_ROWS null as rows',
            'id',
            'domain', 
            '`database`',
            'display_limit',
            'active'
        );
         
        $this->db->from($table_name);
        $return['total_count'] = $this->db->count_all_results();
        
        $this->db->reset_query();
        
        $this->db->select($db_fields, FALSE);
        $this->db->from($table_name);
        $this->db->limit($limit, $start);
        
        $result = $this->db->get();
        
        $return['total_filtered_count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ( $result->num_rows() > 0 ) 
        {
            foreach($result->result_array() as $index => $row) 
            {                
                $return['data'][] = $row;
            }
        }
        
        return $return;
     }
     
     public function get_cronjob_schedule($action)
     {
        $db_fields = array();
         
        $this->db->select();
        $this->db->from($this->table_name);
        $this->db->where('active', 1);
        
        if ($action)
        {
            $this->db->where('action', $action);
        }
        
        $result = $this->db->get();
        if ($result)
        {
            foreach($result as $id => $row)
            {
                $date_added     = (int) trim($row['dateadded']);
                $date_last_run  = (int) trim($row['date_last_run']);
                
                $recurring_type = trim($row['recurring_type']);
                //validate recurring type if not valid default monthly
                $recurring_type = !empty($recurring_type) && in_array($recurring_type, $this->recurring_type_list) ? $recurring_type:'monthly';
                
                
            }
        }
        
     }
     
     public function add_cron_job()
     {
         
     }
 }

