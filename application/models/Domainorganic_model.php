<?php
 class Domainorganic_model extends CI_Model
 {
     private $table_name = '';
     private $dos_db = '';
     protected $_ci;
     protected $domain = '';
     
     public function __construct() 
     {
         parent::__construct();
         $this->_ci =& get_instance();
         $this->dos_db = $this->_ci->load->database('domain_organic_report', TRUE);
     }
     
     public function is_table_exists($table_name = '')
     {
         $query = $this->dos_db->query("SHOW TABLES LIKE '" . (!empty($table_name)?$table_name:$this->table_name). "';");
         $row = $query->row();
         if ($row) {
             return TRUE;
         }
         
         return FALSE;
     }
     
     private function _create_dos_table($table_name)
     {
         $table = (!empty($table_name)?$table_name:$this->table_name);
         
         if (empty($table))
         {
             return FALSE;
         }
         
         $sql_query = "
            CREATE TABLE IF NOT EXISTS `{$table}` (
                `domain` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                `keyword` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                `position` INT(32) NULL DEFAULT NULL,
                `previous_position` INT(32) NULL DEFAULT NULL,
                `search_volume` INT(32) NULL DEFAULT NULL,
                `keyword_difficulty` FLOAT NULL DEFAULT NULL,
                `cpc` FLOAT NULL DEFAULT NULL,
                `url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
                `traffic` FLOAT NULL DEFAULT NULL,
                `traffic_cost` FLOAT NULL DEFAULT NULL,
                `competition` FLOAT NULL DEFAULT NULL,
                `number_of_results` FLOAT NULL DEFAULT NULL,
                `timestamp` INT(32) NULL DEFAULT NULL,
                `date_added` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP
            )
            COLLATE='utf8_unicode_ci'
            ENGINE=MyISAM;";
         
         $this->dos_db->query($sql_query);
     }
     
     public function save_report($report_data)
     {
         
         if ($this->is_table_exists() === FALSE) {
             $this->_create_dos_table();
         }
         
         $this->dos_db->query("TRUNCATE TABLE `".$this->table_name."`");
         
         $csv_db = array(
             '`keyword`' => 'Keyword',
             '`position`' => 'Position',
             '`previous_position`'  => 'Previous Position',
             '`search_volume`' => 'Search Volume',
             '`keyword_difficulty`' => 'Keyword Difficulty Index',
             '`cpc`' => 'CPC',
             '`url`' => 'Url',
             '`traffic`' => 'Traffic (%)',
             '`traffic_cost`' => 'Traffic Cost (%)',
             '`competition`' => 'Competition',
             '`number_of_results`' => 'Number of Results',
             '`timestamp`' => 'Timestamp'
         );
         
         if ($report_data)
         {
            $data = array();
            foreach($report_data as $row) 
            {
                $db_row = array();
                $db_row['`domain`'] = $this->domain;
                foreach($csv_db as $name => $header)
                {
                    $db_row[$name] = isset($row[$header]) ? $row[$header]:NULL;
                }
                if ($db_row) 
                {
                    $data[] = $db_row;
                }
            }
           
            if ($data)
            {
                $this->dos_db->insert_batch($this->table_name, $data);
            }
         }
     }
     
    public function __set($name,$value)
    {
        switch($name)
        {
            case 'table_name':
                $this->set_table_name($value);
                break;
        }
    }
    
    private function set_table_name($value)
    {
        $this->domain = $value;
        $domain_table = str_replace('.', '_', $value);
        $this->table_name = $domain_table;
    }
    
    public function __get($name) 
    {
        switch($name)
        {
            case 'table_name':
                return $this->get_table_name();
        }
    }
    
    private function get_table_name()
    {
        return $this->table_name;
    }
 }

