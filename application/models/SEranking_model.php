<?php
 class SEranking_model extends CI_Model
 {
     private $table_name = '';
     private $dos_db = '';
     protected $_ci;
     protected $domain = '';
     
     public function __construct() 
     {
         parent::__construct();
         $this->_ci =& get_instance();
     }
     
     
     
    public function __set($name,$value)
    {
        switch($name)
        {
            case 'table_name':
                $this->set_table_name($value);
                break;
        }
    }
    
    public function __get($name) 
    {
        switch($name)
        {
            case 'table_name':
                return $this->get_table_name();
        }
    }
 }

