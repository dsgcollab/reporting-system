<?php
class Menu_model extends CI_Model {

	public function all()
	{
		return $this->db->get("menu_items")
					->result_array();
	}
        
        public function get_menu( $menu_name )
        {
            $this->db->select(array('mi.name title', 'mi.*'));
            $this->db->from('menu_items mi');
            $this->db->join('menus m', 'm.menu_id = mi.menu_id');
            $this->db->where('m.name', $menu_name);
            
            $list = $this->db->get()->result_array();
        
            return $this->_prepare_items($list);
        }
        
        private function _prepare_items(array $data, $parent = null)
        {
            $items = array();
            
            foreach($data as $item)
            {
                if ($item['parent'] == $parent)
                {
                    $items[$item['id']] = $item;
                    $items[$item['id']]['children'] = $this->_prepare_items($data, $item['id']);
                }    
            }
            
            usort($items, array($this, 'sort_by_order'));
            return $items;
        } 
        
        private function sort_by_order($a, $b)
        {
            return $a['number'] - $b['number'];
        }
}