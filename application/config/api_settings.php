<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['semrush_api_url']  = 'https://api.semrush.com/';
$config['semrush_api_key']  = '9be9ba79f651aea6590d737570affd29';
$config['domain_organic_api_column_list']  = array(
    'Kd'    => 'Keyword Difficulty',
    'Ph'    => 'Keyword',
    'Po'    => 'Position',
    'Pp'    => 'Previous Position',
    'Cp'    => 'CPC',
    'Nq'    => 'Search Volume',
    'Ur'    => 'URL',
    'Tr'    => 'Traffic',
    'Tc'    => 'Traffic Cost',
    'Co'    => 'Competition',
    'Nr'    => 'Number of Results',
    'Ts'    => 'Timestamp'
);

$config['domain_organic_db'] = 'domain_organic_search_keywords';
$config['linkresearchtool_api_url'] = 'http://app.linkresearchtools.com/toolkit/api.php';
$config['linkresearchtool_api_key'] = '10933491fe84dfd017a17ba21f9b6ca7';

//SERanking Configuration
$config['seranking_api_url'] = ' https://api2.seranking.com/';
$config['seranking_login_details'] = array(
    'username'  => '',
    'password'  => md5('')
);