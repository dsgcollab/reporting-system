<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $this->semrush();
       /*
       $this->breadcrumb->add('Dashboard', base_url()); 
       $this->template->breadcrumbs = $this->breadcrumb->output();
       
       $this->template->page_title = 'Dashboard';
       $this->template->page_header_title = 'Dashboard';
       $this->template->load('dashboard');*/
    }
     
    public function logout()
    {
        if ($this->uri->uri_string() == 'sys/logout')
            show_404();
        
        $logout = $this->ion_auth->logout();
        $this->session->set_flashdata('logout_message', $this->ion_auth->messages());
        redirect('login', 'refresh');
    }
    
    public function semrush()
    {
       ci_add_script(array(
          array(
              //'src' => 'https://cdn.datatables.net/v/bs/jq-3.2.1/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.js',
              'src' => 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
              'position' => 'foot'
          ),
          array(
              'src'  => base_url('assets/js/admin.js'),
              'position' => 'foot'
          )
        ));
        
        ci_add_css(array(
           array(
               'href' => 'https://cdn.datatables.net/v/bs/jq-3.2.1/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.css',
               //'href' => 'https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css',
               'position' => 'head'
           ),
           array(
               'href'       => base_url('assets/css/admin.css'),
               'position'   => 'head'
           )
        )); 
        
       $this->breadcrumb->add('Dashboard', base_url()); 
       $this->template->breadcrumbs = $this->breadcrumb->output();
       
       //GET SEMrush Units Left
       $this->load->model('semrush_model');
       $data['unitsleft'] = $this->semrush_model->getunistleft();
       
       $add = $this->load->view('semrush/add', NULL, TRUE);
        $data['addform'] = $add;
       $this->template->page_title = 'SEMrush';
       $this->template->page_header_title = 'SEMrush';
       $this->template->load('semrush/table', $data);
    }
    
    public function semrushnew()
    {
        ci_add_script(array(
          array(
              //'src' => 'https://cdn.datatables.net/v/bs/jq-3.2.1/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.js',
              //'src' => 'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
              'src' => 'https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js',
              'position' => 'foot'
          ),
          array(
              'src' => 'https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js',
              'position' => 'foot'
          ),
            array(
             'src' => 'https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js',
                'position' => 'foot'
            ),
          array(
              'src'  => base_url('assets/js/admin.js'),
              'position' => 'foot'
          )
        ));
        
        ci_add_css(array(
           array(
               //'href' => 'https://cdn.datatables.net/v/bs/jq-3.2.1/dt-1.10.16/b-1.4.2/cr-1.4.1/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.css',
               //'href' => 'https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css',
               'href'   => 'https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css',
               'position' => 'head'
           ),
           array(
               'href'       => base_url('assets/css/admin.css'),
               'position'   => 'head'
           )
        )); 
        
        $this->page_title   = "SEMrush API";
        $this->h1_title     = "SEMrush API";
        
        $this->content = $this->load->view('semrush/table', NULL, TRUE);
        
        $this->render();
    }
}

