<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publicctrl extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function systemjs()
    {
       header("Content-type: application/x-javascript");
       ?>
       var site_url     = '<?php echo site_url(); ?>';
       var ajax_url = '<?php echo site_url('ajax'); ?>';
       <?php
       exit;
    }
    
    public function phpinfo()
    {
        echo phpinfo();
        exit;
    }
    
     public function login()
    {
        if( $this->uri->uri_string() == 'sys/login' )
            show_404();
        
        if ( $this->ion_auth->logged_in() )
           redirect('/');
        
        
        $login_form_rule_config = array(
           array(
               'field'  => 'identity',
               'label'  => str_replace(':', '', $this->lang->line('login_identity_label')),
               'rules'  => 'required'
           ),
           array(
               'field'  => 'password',
               'label'  => str_replace(':', '',$this->lang->line('login_password_label')),
               'rules'  => 'required'
           )
        ); 
        
        $this->form_validation->set_rules($login_form_rule_config);
        
        if ($this->form_validation->run() === TRUE)
        {
            $remember = (bool)$this->input->post('remember');
            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
            {
                    //if the login is successful
                    //redirect them back to the home page
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    redirect('/', 'refresh');
            }
            else
            {
                    // if the login was un-successful
                    // redirect them back to the login page
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect('login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }else{
            $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $logout_message = $this->session->flashdata('logout_message');
            $data = array(
                'error_message' => $message,
                'logged_out_message' => $logout_message ? $logout_message:FALSE
            );
            
            $this->load->view( config_item('theme') . '/login_page', $data);            
        }        
    }
}