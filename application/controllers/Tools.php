<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function semrush_domain_reports($report_type, $domain, $database)
    {
        switch ($report_type) {
            case 'domain_organic':
                $this->semrushapi->domain_reports('domain_organic', array(
                    'domain'        => $domain,
                    'display_limit' => 1,
                    'database'      => $database
                ));
                break;
            default:
                break;
        }
    }
    
    public function semrush_analytics_api()
    {
       $argv    = isset($_SERVER["argv"]) ? $_SERVER["argv"]:FALSE;
       $type    = isset($argv[3]) ? $argv[3]:FALSE;
       $domain  = isset($argv[4]) ? $argv[4]:FALSE;
       $column  = isset($argv[5]) ? $argv[5]:FALSE;
       
       $api_url     = $this->config->item('semrush_api_url');
       $api_key     = $this->config->item('semrush_api_key');
       $api_column  = $column?$column:$this->config->item('api_column_list');
       
       $url_param = array(
           'type' => $type,
           'export_columns'    => implode(array_keys($api_column), ','),
           'key'               => $api_key,
       );
       
       switch($type) 
       {
           case 'domain_organic':
               if ($domain)
               {
                    $api_settings = $this->do->get_domain_api_settings($domain);
                    if ($api_settings === FALSE) {
                        echo 'Domain Settings Not Found';
                        exit;
                    }
                    
                    $display_limit      = $api_settings->display_limit;
                    $google_database    = $api_settings->database;
                    $type               = $type?$type:'domain_organic';
                    
                    $url_param['display_limit'] = $display_limit;
                    $url_param['domain']        = $display;
                    $url_param['database']      = $google_database;
                    
                    $url = $api_url . '?' . http_build_query($url_param);
                 
                    $csv_raw_data = $this->_curlcall($url);
                    if ($csv_raw_data)
                    {
                        $a_data = $this->_parsecsv($csv_raw_data);
                        if ($a_data)
                        {
                            $this->do->save_report($domain, $csv_data);
                        }
                    }
                    
               }
               break;
       }
    }
    
    private function _curlcall($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $tmpfname = FCPATH.'cookie.txt';
        curl_setopt($ch, CURLOPT_COOKIEJAR, $tmpfname);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $tmpfname);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        $html = $output;
        curl_close($ch);
        
        if ($output)
        {
            
            return $html;
        }
        
        return FALSE;
    }
        
    private function _parsecsv($csv_text = '')
    {
        $return = FALSE;
        $rowlist = str_getcsv($csv_text, "\n\r");
        
        if ($rowlist)
        {
            $header = str_getcsv(array_shift($rowlist), ';');
            
            $i = 0;
            foreach($rowlist as $row) 
            {
               $a_row = str_getcsv($row, ';');
                if ($a_row) {
                    $final_row = array();
                    foreach($header as $index => $value) {
                        $final_row[$value] = $a_row[$index];
                    }
                    $return[] = $final_row;
                }
            }
        }
        
        return $return;
    }
    
    public function updateunitsleft()
    {
        $url = 'http://www.semrush.com/users/countapiunits.html?key=' . $this->config->item('semrush_api_key');
        
        $units = $this->_curlcall($url);
        $this->db->query("TRUNCATE TABLE `unitscount`");
        $this->db->query("INSERT INTO `unitscount` (`unitsleft`) VALUES('".$units."')");
        
        echo var_dump($units);
    }
    
   public function curltest()
   {
       echo var_dump($this->semrushapi->accounts_api('check_api_units_balance'));
   }
   
   public function modeltest($domain)
   {
       $this->load->model('domainorganic_model', 'do');
       $this->do->table_name = $domain;
       echo var_dump($this->do->table_name);
   }
   
   public function semrush_export_report($type, $domain, $limit, $db)
   {
       echo var_dump($type);
       echo var_dump($domain);
       echo var_dump($limit);
       echo var_dump($db);
   }
   
   public function lrttestclass($action, $rid)
   {
       $this->load->library('linkresearchtoolapi');
       echo var_dump($this->linkresearchtoolapi->extract_data_report($action, $rid));
   }
}

