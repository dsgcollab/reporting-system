<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        
    }
    
    public function run()
    {
        $this->load->model('cronschedule_model', 'cron');
        
        $cron_schedule_list = $this->cron->get_cronjob_schedule();
        if ($cron_schedule_list !== FALSE)
        {
            foreach($cron_schedule_list as $key => $cron_schedule)
            {
                switch($cron_schedule['action'])
                {
                    case 'semrushapi':
                        $this->load->libarary('semrushapi');
                        $settings = isset($cron_schedule['settings'])?$cron_schedule['settings']:FALSE;
                        
                        if ($settings)
                        {
                           $this->semrushapi->domain_reports($settings['report_type'], $settings);
                        }
                        break;
                }
            }
        }
        
    }
}

