<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxrequest extends MY_Controller
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        header('Content-Type: application/json');
        $request = $this->input->post('q');
        
        switch( $request )
        {
            case 'loadapisettings':
                $this->load->model('semrush_model', 'sr');
                $draw = (int) $this->input->post('draw');
                
                $response = array(
                    'draw'              => $draw,
                    'recordsTotal'      => 0,
                    'recordsFiltered'   => 0,
                    'error'             => '',
                    'data'              => array()
                );
                
                $list_settings = $this->sr->apisettings();
                
                if ( $list_settings['data'] ) 
                {

                    $data = array();
                    foreach($list_settings['data'] as $row) 
                    {
                        $data[] = array(
                            'DT_RowId'      =>  $row['id'],
                            'id'            =>  $row['id'],
                            'domain'        =>  $row['domain'],
                            'database'      =>  $row['database'],
                            'display_limit' =>  $row['display_limit'],
                            'active'        =>  $row['active'],
                        );
                    }

                    $response['data']               = $data;
            
                }
                
                $response['recordsFiltered']    = $list_settings['total_filtered_count'];
                $response['recordsTotal']       = $list_settings['total_count'];
                
                echo json_encode($response);
                die();
                break;
            case 'loaddomainorganic';
                $response = new stdClass;
                
                $response->draw = (int) $this->input->post('draw');
                $response->recordsTotal     = 0;
                $response->recordsFiltered  = 0;
                $response->error            = '';
                $response->data             = [];
                
                
                echo json_encode($response);
                break;
        }
        exit;
    }      
}